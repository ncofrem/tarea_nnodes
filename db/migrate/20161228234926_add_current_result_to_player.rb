class AddCurrentResultToPlayer < ActiveRecord::Migration[5.0]
  def change
    add_column :players, :current_result, :integer
  end
end
