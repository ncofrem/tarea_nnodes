class CreateBetHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :bet_histories do |t|
      t.references :player, foreign_key: true
      t.datetime :date
      t.string :color_player
      t.integer :bet_player
      t.string :color_roulette
      t.integer :result

      t.timestamps
    end
  end
end
