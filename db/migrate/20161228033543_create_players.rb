class CreatePlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :players do |t|
      t.string :name
      t.string :last_name
      t.integer :cash
      t.string :current_color
      t.integer :current_bet

      t.timestamps
    end
  end
end
