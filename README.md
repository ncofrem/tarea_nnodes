# Tarea-Nnodes versión 1.0 (3/01/2017)

* URL: https://tarea-nnodes.herokuapp.com/
* Correo de contacto: n.cofre.mendez@gmail.com
__________________________________________
**Descripción general:**

* La aplicación es un sistema que simula un juego de ruleta de casino. 
* Se tiene un grupo dinámico de personas jugando. 
* El sistema tiene como funcionalidad el monitorear en tiempo real una mesa de casino, donde los jugadores modifican sus apuestas dependiendo del clima. 
* Pueden agregarse jugadores a la mesa y/o editar las cantidades de dinero que posean. 
* Los participantes son más o menos conservadores al apostar dependiendo del clima pronosticado dentro de los próximos siete días. 
* Todos los participantes comienzan con una cantidad de dinero que será renovada al finalizar un día de juego.
* El sistema mantiene a los participantes realizando jugadas autónomamente cada tres minutos. Dichas jugadas son escogidas aleatoriamente y los participantes juegan hasta que se quedan sin dinero. 

__________________________________________
**Especificaciones técnicas:**

* Aplicación desarrollada en Ruby (versión 2.2.6) con Ruby on Rails (versión 5.0.1).
* Aplicación publicada en el servidor de producción gratuito Heroku.
* El sistema es capaz de ser ejecutado en un sistema UNIX.

__________________________________________
**Instalación local:**

Clonar el repositorio:

* $ git clone https://ncofrem@bitbucket.org/ncofrem/tarea_nnodes.git  
* $ cd tarea_nnodes

Ejecutar comandos:

* bundle install
* bundle update

**Nota:** Posible error en Linux: *"An error occurred while installing pg (0.19.0), and Bundler cannot continue."*

* $ sudo apt-get install libpq-dev

Ejecutamos migraciones:

* $ rake db:migrate

Ejecutamos la aplicación:

* $ rails s

En el navegador de internet ingresamos a:

* localhost:3000

__________________________________________
**Ejecución:**

1. En la barra de navegación seleccionamos *"Jugadores"*.
2. Agregamos los Jugadores que deseamos.
3. En la barra de navegación seleccionamos *"Administración"*.
4. Presionamos en: *"Comenzar el día"* para comenzar con las rondas
5. Nuestro monitoreo comenzara a actualizarse automáticamente cada 3 minutos en la pestaña principal (*"Monitoreo"*).
6. Para finalizar en la pestaña de Administración presionamos en: *"Fin del día"*.