json.extract! player, :id, :name, :last_name, :cash, :current_color, :current_bet, :created_at, :updated_at
json.url player_url(player, format: :json)