class Player < ApplicationRecord

	# Se incluye archivo random_color.rb (Concerns) que contiene funcion de color_random 
	include RandomColor

	# Consulta que busca los jugadores que tienen dinero para apostar
	scope :players_with_cash, -> { where("cash > ?", 0) }


	# Funcion que valida si ya se consumio la API del Clima, busca los jugadores que pueden apostar
	# y realiza las apuestas de los Players
	def self.bet_players color_roulette
	  	
	  	players = Player.players_with_cash

		players.map { |player|

			current_bet_player(player)
			update_cash_and_current_result(player, color_roulette)

		}

		return players

	end

  	# Funcion que modifica Player con su Apuesta Actual
  	def self.current_bet_player player

  		# Si es menor a $1.000 el Player va All in
  		if player.cash <= 1000
  			player.current_bet = player.cash
  		else
  			player.current_bet = random_bet_cash_player(player.cash)
  		end

  		player.current_color = random_color

  	end

  	# Funcion que retorna la cantidad de dinero que el Player apuesta en base al Clima
  	def self.random_bet_cash_player cash_player

		if $weather
  			return ((cash_player * rand(4..10)) / 100).to_i
		end

  		return ((cash_player * rand(8..15)) / 100).to_i

  	end


	# Funcion que Actualiza la cantidad de dinero y apuesta actual del Player
	def self.update_cash_and_current_result player, color_roulette

		player.current_result = result_bet(player, color_roulette)
		player.cash += player.current_result
		player.save

	end

	# Funcion que obtiene el resultado de la apuesta en dinero
	def self.result_bet player, color_roulette

		if player.current_color == color_roulette

			case color_roulette
				when 'Verde'
					return player.current_bet * 15
				else
					return player.current_bet * 2
			end

		end

		return player.current_bet * -1

	end

  	# Funcion que retorna True si en el pronostico de los siguiente 7 dias hay temperaturas
  	# sobre los 32 Grados
  	def self.weather_more_than_thirty_two

  		api_weather.take(7).each do |forecastday|
	
			if (forecastday["high"]["celsius"]).to_i > 32
				return true
			end

		end

		return false

  	end

  	# Funcion que realiza el consumo de la API y retorna el JSON con el pronostico 
  	def self.api_weather

		api_key = "047364c18d25895e"
		url_api = "http://api.wunderground.com/api/#{api_key}/forecast10day/lang:SP/q/CL/Santiago.json"
		json_api = HTTParty.get(url_api)
		return json_api["forecast"]["simpleforecast"]["forecastday"]

  	end

  	# Funcion que agrega $10.000 a todos los jugadores
  	def self.end_of_the_day

  		Player.all.map { |player| 
  			player.cash += 10000
  			player.save
  		}

  	end

end
