class BetHistory < ApplicationRecord
  belongs_to :player
  after_create :send_to_action_cable

  # Se incluye archivo random_color.rb (Concerns) que contiene funcion de random_color 
  include RandomColor

  # Funcion que crea BetHistory que se va a mostrar en el monitoreo
  def self.create_bet_histories players, color_roulette

  	players.each do |player|

	  	bet_history = BetHistory.new(player_id: player.id, 
	  								 date: Time.now, 
	  								 color_player: player.current_color,
	  								 bet_player: player.current_bet,
	  								 color_roulette: color_roulette,
	  								 result: player.current_result)
	  	bet_history.save

  	end

  end

  # Funcion que retorna todo los BetHistory con el nombre completo del jugador
  def self.find_all_with_full_name

    BetHistory.select("(players.name || ' ' || players.last_name) as name, date, color_player, bet_player, color_roulette, result")
               .joins(:player)
               .order("date DESC")

  end

  # Funcion que retorna el nombre completo del jugador
  def full_name
    self.player.name + " " + self.player.last_name
  end

  private

    # Funcion que envia al canal del ActionCable llamado "bet_history" el html renderizado  
    def send_to_action_cable
      ActionCable.server.broadcast "bet_history", to_html
    end

    # Renderiza y retorna el HTML de la vista _bet_history con el nuevo registro ingresado
    def to_html
      ApplicationController.renderer.render(partial: 'bet_histories/bet_history', locals: { bet_history: self })
    end

end
