module RandomColor
	extend ActiveSupport::Concern

	module ClassMethods

		# Funcion que retorna un string en base a un random
		# 1 a 2 de 100 (2%) = Verde
		# 3 a 51 de 100 (49%) = Rojo 
		# 52 a 100 de 100 (49%) = Negro  
		def random_color

			color = "Verde"

			case rand(1..100)
				when 3..51
					color = "Rojo"
				when 52..100
					color = "Negro"
			end

			return color

		end
	end
end