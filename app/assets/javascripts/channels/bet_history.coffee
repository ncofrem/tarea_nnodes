App.bet_history = App.cable.subscriptions.create "BetHistoryChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    $('#bet_histories').prepend data
    startTimer()