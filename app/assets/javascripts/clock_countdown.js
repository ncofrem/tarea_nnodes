function startTimer() {
    var display = document.querySelector('#time');
    var duration = 180
    var timer = duration, seconds;
    setInterval(function () {
        seconds = parseInt(timer);

        display.style.width = (seconds*100) / duration + "%";

        timer--;
    }, 1000);
}