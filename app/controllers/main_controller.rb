class MainController < ApplicationController

  @@thread = Thread.new { }
	
  # GET /
  def index
    @bet_histories = BetHistory.find_all_with_full_name
  end

  # POST /play_game
  def play_game
    
    unless @@thread.alive?
      $weather = Player.weather_more_than_thirty_two 
      @@thread = Thread.new { play_game_thread }
    end
    redirect_to root_path

  end

  # GET /admin_game
  def admin_game
  end

  # POST /stop_game
  def stop_game

  	if @@thread.alive?
      Player.end_of_the_day
  		@@thread.exit
  	end
    redirect_to root_path

  end

  # Funcion que contiene un bucle para que se ejecute cada 3 min las apuestas
  def play_game_thread

  	while true
  		color_roulette = BetHistory.random_color
  		players = Player.bet_players(color_roulette)
  		BetHistory.create_bet_histories(players, color_roulette)
  		sleep(180)  	
  	end

  end

end