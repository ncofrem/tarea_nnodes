class BetHistoryChannel < ApplicationCable::Channel
  def subscribed
    stream_from "bet_history"
  end

  def unsubscribed
  end

end
