Rails.application.routes.draw do

  # Ruta del monitoreo
  root 'main#index'

  resources :players

  # Ruta para Administrar el inicio y el fin de las rondas
  get '/admin_game', to: 'main#admin_game'

  # Ruta para ejecutar el Thread del juego
  post '/play_game', to: 'main#play_game'

  # Ruta para detener el Thread del juego
  post '/stop_game', to: 'main#stop_game'

  # Serve websocket ActionCable
  mount ActionCable.server => '/cable'

end
